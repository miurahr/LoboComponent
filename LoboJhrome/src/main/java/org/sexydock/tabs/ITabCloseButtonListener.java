package org.sexydock.tabs;

import javax.swing.*;

public interface ITabCloseButtonListener {
    void tabCloseButtonPressed(JTabbedPane tabbedPane, int tabIndex);
}
