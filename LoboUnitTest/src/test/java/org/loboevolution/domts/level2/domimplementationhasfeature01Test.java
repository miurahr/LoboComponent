
/*
 * GNU GENERAL LICENSE
 * Copyright (C) 2014 - 2021 Lobo Evolution
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * verion 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General License for more details.
 *
 * You should have received a copy of the GNU General Public
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact info: ivan.difrancesco@yahoo.it
 */

package org.loboevolution.domts.level2;

import org.junit.Test;
import org.loboevolution.driver.LoboUnitTest;
import org.loboevolution.html.dom.nodeimpl.DocumentImpl;
import org.loboevolution.html.node.DOMImplementation;
import org.loboevolution.html.node.Document;

import static org.junit.Assert.assertTrue;


/**
 * The method "hasFeature(feature,version)" tests if the DOMImplementation implements
 * a specific feature and if so returns true.
 * <p>
 * Call the hasFeature method on this DOMImplementation with a combination of features
 * versions as below.  Valid feature names are case insensitive and versions "2.0",
 * "1.0" and if the version is not specified, supporting any version of the feature
 * should return true.  Check if the value returned value was true.
 *
 * @author IBM
 * @author Neil Delima
 * @see <a href="http://www.w3.org/TR/DOM-Level-2-Core/core#ID-5CED94D7">http://www.w3.org/TR/DOM-Level-2-Core/core#ID-5CED94D7</a>
 */
public class domimplementationhasfeature01Test extends LoboUnitTest {

    /**
     * Runs the test case.
     *
     */
    @Test
    public void runTest() {
        DocumentImpl doc;
        DOMImplementation domImpl;
        String version = "";
        String version1 = "1.0";
        String version2 = "2.0";
        String featureCore;
        String featureXML;
        boolean success;
        java.util.List featuresXML = new java.util.ArrayList();
        featuresXML.add("XML");
        featuresXML.add("xmL");

        java.util.List featuresCore = new java.util.ArrayList();
        featuresCore.add("Core");
        featuresCore.add("CORE");

        doc = (DocumentImpl) sampleXmlFile("staffNS.xml");
        doc.setTest(true);
        domImpl = doc.getImplementation();
        for (Object o : featuresXML) {
            featureXML = (String) o;
            success = domImpl.hasFeature(featureXML, version);
            assertTrue("domimplementationhasfeature01_XML_1", success);
            success = domImpl.hasFeature(featureXML, version1);
            assertTrue("domimplementationhasfeature01_XML_2", success);
        }
        for (Object o : featuresCore) {
            featureCore = (String) o;
            success = domImpl.hasFeature(featureCore, version);
            assertTrue("domimplementationhasfeature01_Core_1", success);
            success = domImpl.hasFeature(featureCore, version1);
            success = domImpl.hasFeature(featureCore, version2);
            assertTrue("domimplementationhasfeature01_Core_3", success);
        }
    }
}

