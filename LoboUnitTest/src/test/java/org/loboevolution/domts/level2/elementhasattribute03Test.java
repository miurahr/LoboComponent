
/*
 * GNU GENERAL LICENSE
 * Copyright (C) 2014 - 2021 Lobo Evolution
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * verion 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General License for more details.
 *
 * You should have received a copy of the GNU General Public
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact info: ivan.difrancesco@yahoo.it
 */

package org.loboevolution.domts.level2;

import org.junit.Test;
import org.loboevolution.driver.LoboUnitTest;
import org.loboevolution.html.node.Attr;
import org.loboevolution.html.node.Document;
import org.loboevolution.html.node.Element;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * The method hasAttribute returns true when an attribute with a given name is specified
 * on this element or has a default value, false otherwise.
 * Create an element Node and an attribute Node.  Invoke hasAttribute method
 * to verify that there is no attribute. Append the attribute node to the element node.
 * Invoke the hasAttribute method on the element and verify if it returns true.
 *
 * @author IBM
 * @author Neil Delima
 * @see <a href="http://www.w3.org/TR/DOM-Level-2-Core/core#ID-NodeHasAttrs">http://www.w3.org/TR/DOM-Level-2-Core/core#ID-NodeHasAttrs</a>
 */
public class elementhasattribute03Test extends LoboUnitTest {

    /**
     * Runs the test case.
     *
     */
    @Test
    public void runTest() {
        Document doc;
        Element element;
        boolean state;
        Attr attribute;
        Attr newAttribute;
        doc = sampleXmlFile("staff.xml");
        element = doc.createElement("address");
        attribute = doc.createAttribute("domestic");
        state = element.hasAttribute("domestic");
        assertFalse("elementhasattribute03_False", state);
        newAttribute = element.setAttributeNode(attribute);
        state = element.hasAttribute("domestic");
        assertTrue("elementhasattribute03_True", state);
    }
}

