plugins {
    java
    id("io.freefair.lombok") version "6.5.1"
}

version = "3.1-SNAPSHOT"

repositories {
    mavenCentral()
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

java.sourceSets["main"].java {
    srcDirs(
        "LoboApi/src/main/java",
        "LoboParser/src/main/java",
        "LoboW3C/src/main/java",
        "LoboCommon/src/main/java",
        "LoboStore/src/main/java",
        "LoboIViewer/src/main/java",
        "LoboHTML/src/main/java",
        "LoboPDF/src/main/java",
        "LoboUnitTest/src/main/java"
    )
}
java.sourceSets["main"].resources {
    srcDirs("LoboParser/src/main/resources", "LoboStore/src/main/resources", "LoboPDF/src/main/resources")
}
java.sourceSets["test"].java {
    srcDir("LoboUnitTest/src/test/java")
}
java.sourceSets["test"].resources {
    srcDir("LoboUnitTest/src/test/resources")
}

dependencies {
    implementation("org.xerial:sqlite-jdbc:3.36.0.3")
    implementation("com.fifesoft:rsyntaxtextarea:3.2.0")
    implementation("xalan:xalan:2.7.2")
    implementation("org.glassfish.corba:glassfish-corba-omgapi:4.2.4")
    implementation("org.json:json:20220320")

    testImplementation("junit:junit:4.13.2")
    testCompileOnly("org.projectlombok:lombok:1.18.16")
    testAnnotationProcessor("org.projectlombok:lombok:1.18.16")
}
