<p align="center">
	<img src="https://avatars3.githubusercontent.com/u/6113075?s=460&v=4" height="130">
</p>
<p align="center">	
	<a href="https://www.codacy.com/gh/LoboEvolution/LoboEvolution/dashboard?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=LoboEvolution/LoboEvolution&amp;utm_campaign=Badge_Grade" alt="Codacy">
		<img src="https://app.codacy.com/project/badge/Grade/899f68bba4a5463d8a7699821d840c5c" />
	</a>
	<a href="https://codebeat.co/projects/github-com-loboevolution-loboevolution-master"><img alt="codebeat badge" src="https://codebeat.co/badges/74e4393e-77b9-44a9-ad98-0b33fb839754" /></a>
	<a href="https://codeclimate.com/github/LoboEvolution/LoboEvolution/maintainability"><img src="https://api.codeclimate.com/v1/badges/eaeed65cfc69b72b4701/maintainability" /></a>
	<a href="https://github.com/LoboEvolution/LoboEvolution/actions/workflows/codeql.yml">
		<img src="https://github.com/LoboEvolution/LoboEvolution/actions/workflows/codeql.yml/badge.svg?branch=master" alt="Docs">
	</a>
</p>
<p align="center">
	<a href="http://sourceforge.net/projects/loboevolution/">
		<img src="https://img.shields.io/website-up-down-green-red/http/shields.io.svg" alt="Website">
	</a>
	<a href="" alt="PRs">
		<img src="https://img.shields.io/badge/PRs-welcome-brightgreen.svg" />
	</a>
	<a href="https://github.com/oswetto/LoboEvolution/commits/master">
		<img src="https://img.shields.io/badge/Maintained%3F-yes-green.svg" alt="Maintenance">
	</a>
	<a href="https://loboevolution.github.io/LoboEvolution/">
		<img src="https://inch-ci.org/github/oswetto/LoboEvolution.svg" alt="Docs">
	</a>
</p>
<p align="center">	
	<a href="https://github.com/oswetto">
		<img src="https://img.shields.io/badge/Ask%20me-anything-1abc9c.svg" alt="Ask Me">
	</a>
	<a href="https://github.com/LoboEvolution/LoboEvolution/blob/master/LICENSE">
		<img src="https://img.shields.io/badge/License-GPLv3-blue.svg" alt="GPLv3 license">
	</a>	
	<a href="https://search.maven.org/search?q=g:%22com.github.oswetto%22%20AND%20a:%22loboevolution%22">
		<img src="https://img.shields.io/maven-central/v/com.github.oswetto/loboevolution.svg?label=Maven%20Central" alt="Maven Central">
	</a>
	<a href="https://github.com/LoboEvolution/LoboEvolution/actions/workflows/publish-github.yml">
		<img src="https://github.com/LoboEvolution/LoboEvolution/actions/workflows/publish-github.yml/badge.svg" alt="Github Package">
	</a>
</p>

Lobo Evolution began as a fork of the now-defunct project called the LoboBrowser [credits](https://sourceforge.net/projects/xamj/).


### Supports
Lobo Evolution is an extensible all-Java web browser and RIA platform. <br/>
[![ForTheBadge uses-js](http://ForTheBadge.com/images/badges/uses-js.svg)](http://ForTheBadge.com)
[![ForTheBadge uses-html](http://ForTheBadge.com/images/badges/uses-html.svg)](http://ForTheBadge.com)
[![ForTheBadge uses-css](http://ForTheBadge.com/images/badges/uses-css.svg)](http://ForTheBadge.com) 

### Download
 
Last release click Download Badge <br/> 
<a href="https://github.com/LoboEvolution/LoboEvolution/releases/download/3.0/loboevolution-3.0.jar">
	<img src="https://img.shields.io/github/downloads/LoboEvolution/LoboEvolution/total.svg" alt="Download">
</a> <br/>

After download to run
```
java -jar loboevolution-3.0.jar
```

### Local Building
Run 
[PlatformInit.java](https://github.com/oswetto/LoboEvolution/blob/master/LoboEvo/src/main/java/org/loboevolution/init/PlatformInit.java)

### Maven Users
```
mvn install -Dmaven.test.skip=true
```

### Maven Unit Test & report
```
mvn surefire-report:report
```

| Unit Tests | Passed | Errors | Failures | Rate Success |                        Result                         |
|:----------:|:------:|:------:|:--------:|:------------:|:-----------------------------------------------------:|
|    2520    |  1552  |   27   |   941    |   61,587%    | [see](https://loboevolution.github.io/LoboEvolution/) |

### How To Do
[TODO](https://github.com/oswetto/LoboEvolution/wiki/How-TODO)

### Pre-History
The history of all commits that transform LoboBrowser in LoboEvoluiton is available in [a separate repository](https://github.com/oswetto/LoboEvolutionPreHistory).

### Contributors
![Your Repository's Stats](https://contrib.rocks/image?repo=LoboEvolution/LoboEvolution)